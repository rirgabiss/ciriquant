# CIRIquant

CIRIquant is a comprehensive analysis pipeline for circRNA detection and quantification in RNA-Seq data.

Authors: Jinyang Zhang(zhangjinyang@biols.ac.cn), Fangqing Zhao(zhfq@biols.ac.cn)
Maintainer: Jinyang Zhang

Release Notes Version 0.2.0: Fixed bug in prep_CIRIquant, add support for UCSC & Ensembl annotation file Version 0.1.0: Add correction for RNase R treatment License: The code is released under the MIT License. See the LICENSE file for more detail.

For more information on how to run CIRIquant see: https://sourceforge.net/projects/ciri/files/CIRIquant/

Docker file and image was created by Carina Grøntved Jønck, Bioinformatician, University of Copenhagen

The docker image contains CIRIquant and all needed dependencies:

* CIRIquant
* python 2.7
* bwa
* samtools
* hisat2
* stringtie
* htslib



## Docker 

Retrieve the image:
```bash
$ docker pull bioinformatician/ciriquant_v0.2.0
```

To start a container:
```bash
$ docker container run --rm -it bioinformatician/ciriquant_v0.2.0
```

To start a container with local directory mounted to container's strinngs directory run this instead:
```bash
$ docker container run -v /path/to/local/directory:/usr/src/app/directory_name --rm -it bioinformatician/ciriquant_v0.2.0
```

To close the container:
```bash
$ exit
```

To remove the image:
```bash
# Get IMAGE ID
$ docker image ls

# To remove image:
$ docker image rm IMAGE_ID
```

#### Testing:

Test data is large and should only be downloaded manually for testing:
```bash
$ cd /usr/src/app/CIRIquant
$ wget https://sourceforge.net/projects/ciri/files/CIRIquant/test_data.tar.gz
$ tar -xvf test_data.tar.gz && rm test_data.tar.gz
```

Index and other files for reference genome are missing, so they must be created first:
```bash
$ cd /usr/src/app/CIRIquant/test_data/quant
$ bwa index -a bwtsw chr1.fa
$ hisat2-build chr1.fa chr1.fa
```

Then test that the test data works:
```bash
$ CIRIquant -t 4 -1 ./test_1.fq.gz -2 ./test_2.fq.gz --config ./chr1.yml --no-gene -o ./test -p test
```
